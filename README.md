# loopback-authorization-extension

A LoopBack 4 component for authorization support.

## Installation

```shell
npm install --save loopback-authorization-extension
```

## Basic use

Start by decorating your controller methods with `@authorize` to require the
request to be authorized.

```ts
// src/controllers/test.controller.ts
import {
    AuthorizationBindings,
    authorize
} from "loopback-authorization-extension";
import { get } from "@loopback/rest";

export class TestController {
    @authorize({
        and: [
            async (request: Request, controller: any, methodArgs: any[]) => {
                return true;
            },
            async (request: Request, controller: any, methodArgs: any[]) => {
                return false;
            }
        ]
    })
    @get("/test")
    test(): string {
        return "authorized";
    }
}
```

In order to perform authorization, we need to implement a custom Sequence
invoking the authorization at the right time during the request handling.

```ts
// src/sequence.ts
import {
    RestBindings,
    SequenceHandler,
    FindRoute,
    ParseParams,
    InvokeMethod,
    Send,
    Reject,
    RequestContext
} from "@loopback/rest";
import { inject } from "@loopback/context";
import {
    AuthorizationBindings,
    AuthorizeFn
} from "loopback-authorization-extension";

const SequenceActions = RestBindings.SequenceActions;

export class MySequence implements SequenceHandler {
    constructor(
        @inject(SequenceActions.FIND_ROUTE) protected findRoute: FindRoute,
        @inject(SequenceActions.PARSE_PARAMS)
        protected parseParams: ParseParams,
        @inject(SequenceActions.INVOKE_METHOD) protected invoke: InvokeMethod,
        @inject(SequenceActions.SEND) protected send: Send,
        @inject(SequenceActions.REJECT) protected reject: Reject,
        @inject(AuthorizationBindings.AUTHORIZE_ACTION)
        protected authorizeRequest: AuthorizeFn
    ) {}

    async handle(context: RequestContext) {
        try {
            const { request, response } = context;
            const route = this.findRoute(request);
            const args = await this.parseParams(request, route);

            // This is the important line added to the default sequence implementation, after `this.findRoute` and `this.parseParams` methods
            await this.authorizeRequest(request, args);

            const result = await this.invoke(route, args);
            this.send(response, result);
        } catch (error) {
            this.reject(context, error);
        }
    }
}
```

Finally, put it all together in your application class:

```ts
// src/application.ts
import { BootMixin, Binding, Booter } from "@loopback/boot";
import { RestApplication, RestServer, RestBindings } from "@loopback/rest";
import { AuthorizationComponent } from "loopback-authorization-extension";
import { MySequence } from "./sequence";
import { ApplicationConfig } from "@loopback/core";

export class MyApp extends BootMixin(RestApplication) {
    constructor(options?: ApplicationConfig) {
        super(options);

        this.projectRoot = __dirname;

        this.component(AuthorizationComponent);

        this.sequence(MySequence);
    }

    async start() {
        await super.start();

        const server = await this.getServer(RestServer);
        const port = await server.get(RestBindings.PORT);
        console.log(`REST server running on port: ${port}`);
    }
}
```

Now you can try your protected controller

## Contributions

-   [KoLiBer](http://koliber.ir)

## License

MIT
