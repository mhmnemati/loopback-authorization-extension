import { Request } from "@loopback/rest";

/**
 * interface definition of a function which accepts a request
 * and authorizes user
 */
export interface AuthorizeFn {
    (request: Request, methodArgs: any[]): Promise<void>;
}

/**
 * Authorizer `Where` type system and authorization metadata
 */
export type Where = And | Or | Condition;
export type And = { and: Where[] };
export type Or = { or: Where[] };
export type Condition = AsyncAuthorizer;
export type AsyncAuthorizer = (
    request: Request,
    controller: any,
    methodArgs: any[]
) => Promise<boolean>;
